package de.horscht.main;

import org.junit.Assert;
import org.junit.Test;

import de.horscht.common.Edge;
import de.horscht.common.Graph;
import de.horscht.common.Node;
import de.horscht.common.Weight;

public class GraphTest {

	private static final Node<Integer> TEST_NODE_A = new IntegerNode(50);
	private static final Node<Integer> TEST_NODE_B = new IntegerNode(30);
	private static final Weight TEST_WEIGHT = new IntegerWeight(40);

	private static final Edge<Node<Integer>, Weight> TEST_EDGE = new IntegerEdge(
			TEST_NODE_A, TEST_NODE_B, TEST_WEIGHT);

	private final Graph<Integer> graph;

	public GraphTest() {
		graph = null;
	}

	@Test
	public void testAddNode() {
		graph.addEdge(TEST_EDGE);
	}

	@Test
	public void testTraverse() {
		Assert.assertNotNull(graph.traverse());

		graph.clear();

		Assert.assertTrue(graph.traverse().size() == 0);

		graph.addEdge(TEST_EDGE);

		Assert.assertTrue(graph.traverse().size() > 0);
	}

	@Test
	public void testFindRoute() {
	}

	@Test
	public void testHasCycles() {
	}

	private static class IntegerNode implements Node<Integer> {
		private final int value;

		public IntegerNode(int i) {
			value = i;
		}

	}

	private static class IntegerWeight implements Weight {
		private final int value;

		public IntegerWeight(int i) {
			value = i;
		}

		public int compareTo(Weight weight) {
			// TODO Auto-generated method stub
			return 0;
		}

	}

	private static class IntegerEdge implements Edge<Node<Integer>, Weight> {

		public IntegerEdge(Node<Integer> testNodeA, Node<Integer> testNodeB,
				Weight testWeight) {
			// TODO Auto-generated constructor stub
		}

		public Node<Node<Integer>> getStartNode() {
			// TODO Auto-generated method stub
			return null;
		}

		public Node<Node<Integer>> getEndNode() {
			// TODO Auto-generated method stub
			return null;
		}

		public boolean hasDirection() {
			// TODO Auto-generated method stub
			return false;
		}

		public IntegerWeight getWeight() {
			// TODO Auto-generated method stub
			return null;
		}

	}
}
