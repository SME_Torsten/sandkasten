package de.horscht.common;

public interface Edge<N extends Node<?>> {
	
	N getStartNode();
	
	N getEndNode();
	
	boolean hasDirection();
	
	Weight getWeight();
}
