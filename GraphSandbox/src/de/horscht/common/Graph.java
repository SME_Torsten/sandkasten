package de.horscht.common;

import java.util.Collection;

public interface Graph<T extends Node<?>, U extends Edge<T>> {
	
	void addEdge(U node);
	
	Collection<T> traverse();
	
	Collection<T> findRoute(RoutingStrategy<T> strategy);
	
	boolean hasCycles();

	void clear();
	
	Collection<U> getEdges();
	
	Collection<T> getNodes();
}
