package de.horscht.common;

public interface Weight extends Comparable<Weight>{

	public int compareTo(Weight weight);
}
