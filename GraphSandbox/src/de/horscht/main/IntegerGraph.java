package de.horscht.main;

import java.util.Collection;
import java.util.HashSet;

import de.horscht.common.Graph;
import de.horscht.common.RoutingStrategy;

public class IntegerGraph implements Graph<IntegerNode, IntegerEdge> {
	
	private final Collection<IntegerNode> nodes = new HashSet<>();
	private final Collection<IntegerEdge> edges = new HashSet<>();

	@Override
	public void addEdge(IntegerEdge edge) {
		if(nodes.contains(edge.getStartNode())){
			nodes.add(edge.getStartNode());
		}
		if(nodes.contains(edge.getEndNode())){
			nodes.add(edge.getEndNode());
		}
		if(!edges.contains(edge)){
			edges.add(edge);
		}
	}

	public Collection<IntegerNode> traverse() {
		// TODO Auto-generated method stub
		return null;
	}

	public Collection<IntegerNode> getNodes() {
		return nodes;
	}

	public Collection<IntegerEdge> getEdges() {
		return edges;
	}

	public Collection<IntegerNode> findRoute(
			RoutingStrategy strategy) {
		return null;
	}

	public boolean hasCycles() {
		return false;
	}

	public void clear() {
		nodes.clear();
		edges.clear();
	}


}
